<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use
    App\Http\Requests\ContactRequest;
use App\Models\Contact;


class ContactController extends Controller
{
    //
    public function submit(ContactRequest $req)
    {
        # code...
        $contact = new Contact();
        $contact->name = $req->input('name');
        $contact->email = $req->input('email');
        $contact->subj = $req->input('subj');
        $contact->msg = $req->input('msg');
        $contact->save();
        return redirect()->route('home')->with('success', 'All added');
    }
    public function allData()
    {
        // ->take(2) limit 
        // ->skip(2) skip 
        // ->where('subj', '=', 'hi') where 
        return view('messages', ['data' => Contact::all()]);
    }
    public function showOneMessage($id)
    {

        return view('one-message', ['data' => Contact::find($id)]);
    }
    public function updateOneMessage($id)
    {

        return view('update-message', ['data' => Contact::find($id)]);
    }
    public function deleteOneMessage($id)
    {
        Contact::find($id)->delete();
        return redirect()->route('contact-data')->with('success', 'Massage deleted');
    }
    public function updateOneMessageSubmit($id, ContactRequest $req)
    {
        $contact = Contact::find($id);
        $contact->name = $req->input('name');
        $contact->email = $req->input('email');
        $contact->subj = $req->input('subj');
        $contact->msg = $req->input('msg');
        $contact->save();
        return redirect()->route('contact-one', $id)->with('success', 'All updated');
    }
}
