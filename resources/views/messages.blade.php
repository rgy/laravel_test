@extends('lays.app')
@section('title') Title messages @endsection
@section('content')
<h1>Messages</h1>

@foreach ($data as $el)
<div class="alert alert-info">
    <h4>{{ $el->subj }}</h4>
    <p>{{ $el->email }} - {{ $el->name }}</p>
    <p><small>{{ $el->created_at }} </small></p>
    <a href="{{ route('contact-one', $el->id) }}"><button class="alert alert-warning">Details</button></a>
</div>
@endforeach


@endsection