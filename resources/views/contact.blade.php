@extends('lays.app')
@section('title')Title cont @endsection
@section('content')
<h1>Contact</h1>
<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Totam quo nostrum nesciunt recusandae quidem neque sapiente laudantium tempore eligendi ipsa amet doloremque quaerat id, delectus exercitationem quae cumque repellendus vitae.</p>



<form action="{{ route('contact-form') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Enter name</label>
        <input type="text" name="name" placeholder="Enter name" id="name" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Enter email</label>
        <input type="text" name="email" placeholder="Enter email" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="subj">Enter subj</label>
        <input type="text" name="subj" placeholder="Enter subj" id="subj" class="form-control">

    </div>
    <div class="form-group">
        <label for="msg">Enter msg</label>
        <textarea name="msg" placeholder="Enter msg" id="msg" class="form-control" placeholder="Enter msg">

    </textarea>

    </div>
    <button type="submit" class="btn btn-success">Send</button>

</form>

@endsection