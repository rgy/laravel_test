@extends('lays.app')
@section('title') {{$data->subj}} @endsection
@section('content')
<h1>Message</h1>


<div class="alert alert-info">

    <p>{{ $data->msg }}</p>
    <p>{{ $data->email }} - {{ $data->name }}</p>
    <p><small>{{ $data->created_at }} </small></p>
    <a href="{{ route('contact-update', $data->id) }}"><button class="alert alert-primary">Edit</button></a>
    <a href="{{ route('contact-delete', $data->id) }}"><button class="alert alert-danger">Delete</button></a>
</div>



@endsection