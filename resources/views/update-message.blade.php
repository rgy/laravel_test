@extends('lays.app')
@section('title')Update {{$data->subj}} @endsection
@section('content')
<h1>Edit</h1>


<form action="{{ route('contact-update-submit', $data->id) }}" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Enter name</label>
        <input type="text" name="name" value="{{$data->name}}" placeholder="Enter name" id="name" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Enter email</label>
        <input type="text" name="email" value="{{$data->email}}" placeholder="Enter email" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="subj">Enter subj</label>
        <input type="text" name="subj" value="{{$data->subj}}" placeholder="Enter subj" id="subj" class="form-control">

    </div>
    <div class="form-group">
        <label for="msg">Enter msg</label>
        <textarea name="msg" placeholder="Enter msg" id="msg" class="form-control" placeholder="Enter msg">{{$data->msg}}</textarea>

    </div>
    <button type="submit" class="btn btn-success">Update</button>

</form>

@endsection