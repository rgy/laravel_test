<?php

use App\Http\Controllers\ContactController;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/contact/all/{id}', [ContactController::class, 'showOneMessage'])->name('contact-one');
Route::get('/contact/all/{id}/delete', [ContactController::class, 'deleteOneMessage'])->name('contact-delete');
Route::get('/contact/all/{id}/update', [ContactController::class, 'updateOneMessage'])->name('contact-update');
Route::post('/contact/all/{id}/update', [ContactController::class, 'updateOneMessageSubmit'])->name('contact-update-submit');
Route::get('/contact/all', [ContactController::class, 'allData'])->name('contact-data');
Route::post('/contact/submit', [ContactController::class, 'submit'])->name('contact-form');
